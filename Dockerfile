FROM openshift/base-centos7

MAINTAINER Christoph Görn <goern@b4mad.net>

# the license lable makes a statement about this Dockerfile not the packages software
LABEL name="This is a CentOS7 Java 1.8 OpenJDK container base image" \
      vendor="#B4mad.Net" \
      license="GPLv3" \
      io.k8s.display-name="CentOS7 Java 1.8 OpenJDK" \
      io.openshift.tags="java18" \
      version="1.0.0"

RUN INSTALL_PKGS="java-1.8.0-openjdk java-1.8.0-openjdk-devel" && \
    yum install -y --enablerepo=centosplus --setopt=tsflags=nodocs $INSTALL_PKGS && \
    rpm -V $INSTALL_PKGS && \
    yum update-minimal -y --security --sec-severity=Important --sec-severity=Critical --setopt=tsflags=nodocs && \
    yum clean all && \
    rm -rf /var/cache/yum/*

ENV BUILD_VERSION 1
LABEL build-date="2017-04-20" \
      release="1"
